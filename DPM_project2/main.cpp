#include "FastDPM.h"
#include <stdlib.h>  
#include <stdio.h>  
#include <math.h>  
#include <cstdlib>
#include <shlwapi.h>
#include <cv.h>  
#include <highgui.h>  
#include <iostream>  
#include <string>  
#include <conio.h>
#include <opencv2/core/core.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv2/imgproc/imgproc.hpp>  
#include <opencv2/objdetect/objdetect.hpp>  
#include <opencv2/ml/ml.hpp>  
using namespace std;
using namespace cv;

static int NUM_FRAME;
static int s_frameH;
static int s_frameW;
static char* video;
static char*file; 

vector<string>  yuStdDirFiles( string DirName, vector<string> FileExtensions );
string trim_file_name( string FileName, int FLAG );

void Video_to_image(char* file)
{
	printf("------------- video to image ... ----------------\n");
	//初始化一个视频文件捕捉器  
	//CvCapture* capture = cvCaptureFromAVI(file);  
	CvCapture* capture = cvCaptureFromFile(file);

	//获取视频信息  
	cvQueryFrame(capture);
	s_frameH = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);
	s_frameW = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
	int fps = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);
	int numFrames = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);

	printf("\tvideo height : %d\n\tvideo width : %d\n\tfps : %d\n\tframe numbers : %d\n", s_frameH, s_frameW, fps, numFrames);

	//定义和初始化变量  
	int i = 0;
	IplImage* img = 0;
	char image_name[256];

	cvNamedWindow("mainWin", CV_WINDOW_AUTOSIZE);

	//读取和显示  
	while (1)
	{

		img = cvQueryFrame(capture); //获取一帧图片  
		cvShowImage("mainWin", img); //将其显示  
		char key = cvWaitKey(20);

		sprintf(image_name, "C:\\picture\\%d%s", ++i, ".jpg");//保存的图片名  

		cvSaveImage(image_name, img);   //保存一帧图片  

		if (i == NUM_FRAME) break;
	}

	cvReleaseCapture(&capture);
	cvDestroyWindow("mainWin");
}

int	main()
{
	file = "C:\\test\\test.avi";
	char filename[256];
	char filename1[256];

	cout << " 进入视频转图片部分  " << endl;
	cout << "                 " << endl;
	//Video_to_image(file); //视频转图片  


	cout << "                 " << endl;
	cout << " 进入主体检测部分  " << endl;
	cout << "                 " << endl;

	waitKey(3000);
	
	/* I. Get images */
	string	img_dir = "C:/picture/";
	string extensions[] = { ".jpg" };
	vector<string>	img_extensions( extensions, extensions+1 );
	vector<string>	imgnames = yuStdDirFiles( img_dir, img_extensions );
 	//imgnames.clear();
	// imgnames.push_back("000034.jpg");
	imgnames.push_back(img_dir);

	/* II. Perform Part_Model based detection */

	FastDPM	PM("model_inriaperson.txt" );
	//FastDPM	PM("model.txt");

	bool	flag = false;
	for( unsigned i=1; i<imgnames.size(); i++ ){
		string	img_name = imgnames[i];
		Mat	img_uint8 = imread( img_name.c_str() );
	
		
		if( img_uint8.empty() ){
			cout<<"Cannot get image "<<img_name<<endl;
			getchar();
			
			return -2;
		}
		cout<<"Processing "<<trim_file_name(img_name,0)<<endl;
		string a = trim_file_name(img_name, 0);
		Mat	img = PM.prepareImg( img_uint8 );
		PM.detect( img, -1.0f, true, true );	
		cout<<"------------------------------------------------------------"<<endl;
		if( PM.detections.empty() )
			continue;
		flag = true;
		char key = waitKey(1);
		if( key==27 )
			break;
		//	sprintf(filename, "%s/%d.jpg", "C:/picture3", i);
		sprintf(filename, "%s%s", "C:/picture/", a.c_str());
		Mat src = imread(filename);
		IplImage image = IplImage(src);
		sprintf(filename1, "%s/%s", "C:/picture2",a.c_str() );
		cvSaveImage(filename1, &image);

	}
	cout<<"Finished!"<<endl;
	if( flag )
		waitKey();
	else
		_getch();

	return	0;
}
